import React from 'react'
import Tweet from './Tweet'


const ListTweet = ({ data, deleteTweet }) => {


    return <div>
        {
            data.map((tweet, index) => (<Tweet key={index} data={tweet} deleteTweet={deleteTweet} />))
        }
    </div>

}

export default ListTweet;