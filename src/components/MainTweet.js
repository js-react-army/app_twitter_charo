import React, { useState, useEffect } from 'react'

import {
    Card,
    TextField,
    Avatar,
    Grid,
    Button,
    Fab,
} from '@material-ui/core'

// icon
import AddPhotoAlternateIcon from "@material-ui/icons/AddPhotoAlternate";
import ClearIcon from '@material-ui/icons/Clear';

// style
import { makeStyles } from '@material-ui/core/styles'
const useStyles = makeStyles((theme) => ({
    headerLogo: {
        display: 'flex',
        alignItems: 'left',
        margin: '2px',
        width: 'auto'

    },
    toolbar: {
        padding: 0,
    },
}))

//Composant
const MainTweet = ({ handleAjout, idNext }) => {


    const [state, setState] = React.useState({
        id: idNext,
        tweetText: "",
        tweetImage: null,
    })


    const [loading, setLoading] = useState(false)

    const classes = useStyles()

    const uploadedImage = React.useRef(null);
    const imageUploader = React.useRef(null);


    /**
     * fonction qui part une reference sur une img affiche l image seletionne
     * puis set l image dans l etat
     * @param {event} e 
     */
    const handleImageUpload = e => {

        e.preventDefault()

        const [file] = e.target.files;
        if (file) {
            const reader = new FileReader();
            const { current } = uploadedImage;
            if (current !== null) {
                current.file = file;
                reader.onload = (e) => {
                    current.src = e.target.result;
                    setLoading(true)
                    setState({
                        ...state,
                        tweetImage: e.target.result
                    })
                }
                reader.readAsDataURL(file);
            }
        }
    };

    /**
     * fonction qui gére la saisie du text
     * @param {event} event 
     */
    const onChangeText = (event) => {
        event.preventDefault()
        setState({
            ...state,
            tweetText: event.target.value
        })
    }


    /**
     * fonction qui supprime la photo en apercu
     */
    const onClickSupprPhoto = () => {
        const { current } = uploadedImage;
        //current.preventDefault()
        if (current !== null) {
            current.file = null;
            current.src = "";

        }
        setState({
            ...state,
            tweetImage: null
        })

    }

    return <Card>
        <Grid
            container
            direction="row"
            justify="space-between"
            alignItems="center"
            style={{ justifyContent: 'flex-start', margin: '10px' }}>

            <Grid item className={classes.headerLogo} xs={2} >
                <Avatar src="/images/arthur.jpg" size={100} backgroundcolor="rgba(0,0,0,0)" style={{ border: 0 }} />
            </Grid>
            <Grid item className={classes.headerLogo} xs={9} >
                <TextField

                    id="mainTwitText"
                    name="mainTwitText"
                    placeholder="What'on your mind ?"
                    color="primary"
                    fullWidth
                    size="small"
                    value={state.tweetText}
                    onChange={onChangeText}
                />
            </Grid>
            <Grid item xs={3} >
                <input
                    type="file"
                    accept="image/*"
                    onChange={handleImageUpload}
                    src={state.tweetImage}
                    ref={imageUploader}
                    style={{
                        display: "none"
                    }}
                />
                <label htmlFor="contained-button-file">
                    <Fab component="span" className={classes.button} onClick={() => imageUploader.current.click()} >
                        <AddPhotoAlternateIcon />
                    </Fab>
                </label>
            </Grid>
            <Grid item xs={4} >
                <img
                    alt=""
                    ref={uploadedImage}
                    style={{
                        maxWidth: '150px',
                        maxHeight: '150px',
                        position: "relative"
                    }} />
            </Grid>

            <Grid item xs={2} >
                {loading &&
                    <Button variant="contained" style={{ margin: '10px' }} onClick={onClickSupprPhoto}>
                        <ClearIcon />
                    </Button>}
            </Grid>
            <Grid item xs={2} >
                <Button variant="contained"
                    style={{ justifyContent: 'flex-end' }}
                    disabled={state.tweetImage === null && state.tweetText === ""}
                    onClick={() => { onClickSupprPhoto(); handleAjout(state.tweetImage, state.tweetText); }}> TWEET </Button>
            </Grid>
        </Grid>


    </Card >

}

export default MainTweet;