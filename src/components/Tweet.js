import React, { useState, useEffect } from 'react'

import {
    Card,
    Avatar,
    Grid,
    Typography,
    Fab,
    Tooltip,
} from '@material-ui/core'

import moment from "moment";

import ReplyIcon from '@material-ui/icons/Reply';
import FavoriteIcon from '@material-ui/icons/Favorite';
import FavoriteBorderIcon from '@material-ui/icons/FavoriteBorder';
import DeleteIcon from '@material-ui/icons/Delete';

import { makeStyles } from '@material-ui/core/styles'

const useStyles = makeStyles((theme) => ({
    headerLogo: {
        display: 'flex',
        alignItems: 'left',
        margin: '2px',
        width: 'auto'

    },
    toolbar: {
        padding: 0,
    },
    tooltip: {
        backgroundColor: "transparent",
        color: theme.palette.common.black
    }
}))



const Tweet = ({ data , deleteTweet}) => {

    const { id, pseudo, mail, photo, tweetText, tweetImage, nbLike, owner } = data
    const [liker, setLiker] = useState(false)
    const [repondu, setrepondu] = useState(false)

    const [nbLikeCurrent, setnbLikeCurrent] = useState(nbLike)

    const classes = useStyles()

    // A la MAJ d'un like on set un nouveau nb de like
    useEffect(() => {
        setnbLikeCurrent(nbLike)
    }, [liker])


    /**
     * fonction qui toggle le like ( icone + ajout de 1 like)
     */
    const toggleLike = () => {
        setLiker(!liker)

        if (!liker) {
            setnbLikeCurrent(nbLikeCurrent + 1)
        } else {
            setnbLikeCurrent(nbLike)
        }
    }

    /**
     * fonction qui toggle la reponse a un tweet
     */
    const toggleRepondu = () => {
        setrepondu(!repondu)
    }

    /**
     * fonction qui affiche le bon libelle du nombre de like selon Million , milliers ou moins
     */
    const afficheLike = () => {

        let likeCurrent = nbLike + (liker ? 1 : 0)

        if (likeCurrent > 1000000) {
            likeCurrent = `${Math.round((nbLike + (liker ? 1 : 0)) / 1000000)} m `
        } else if (likeCurrent > 1000) {
            likeCurrent = `${Math.round((nbLike + (liker ? 1 : 0)) / 1000)} k `
        } else {
            likeCurrent = `${(nbLike + (liker ? 1 : 0))} `
        }
        return likeCurrent

    }


    return <Card>
        <Grid
            container
            direction="row"
            justify="space-between"
            alignItems="center"
            style={{ justifyContent: 'flex-start', margin: '10px' }}
        >
            {/* avatar +  pseudo + mail + date*/}
            <Grid item className={classes.headerLogo} xs={2} >

                <Avatar src={`/images/${photo}`} size={100} backgroundcolor="rgba(0,0,0,0)" style={{ border: 0 }} />
            </Grid>
            <Grid item xs={2} >
                <Typography variant="h5" gutterBottom> {pseudo}</Typography>
            </Grid>
            <Grid item xs={3} >
                <Typography variant="h6" gutterBottom> {mail}</Typography>
            </Grid>
            <Grid item xs={4} >
                <Typography variant="body2" gutterBottom> {moment().format('MMM D, YYYY h:mm a')}</Typography>
            </Grid>

            {/* Ligne photo + Text */}
            <Grid item xs={1} ></Grid>
            <Grid item xs={5} >
                <img
                    alt=""
                    src={tweetImage}
                    style={{
                        maxWidth: '150px',
                        maxHeight: '150px',
                        position: "relative"
                    }} />

            </Grid>
            <Grid item xs={5} >
                <Typography variant="body2" gutterBottom> {tweetText} </Typography>
            </Grid>




            {/* like et follow et bouton suppr */}
            <Grid item xs={2} ></Grid>
            <Grid item xs={4} >
                <Tooltip title="Repondre" aria-label="Repondre" onClick={toggleRepondu}>
                    <Fab >
                        {repondu && <ReplyIcon fontSize="default" style={{ color: 'green' }} />}
                        {!repondu && <ReplyIcon fontSize="default" />}
                    </Fab>
                </Tooltip>
            </Grid>

            <Grid item xs={4} style={{ textAlign: 'end' }}>
                <Tooltip title="Like" aria-label="Like" onClick={toggleLike} >
                    <Fab >
                        {liker && <FavoriteIcon fontSize="default" color="secondary" />}
                        {!liker && <FavoriteBorderIcon fontSize="default" />}

                    </Fab>
                </Tooltip>
                {afficheLike()}
            </Grid>
            <Grid item xs={2} >
                {owner &&
                    <Tooltip title="Delete" aria-label="Delete" onClick={() => deleteTweet(id )} className={classes.tooltip}>
                        <Fab >
                            <DeleteIcon fontSize="small" />
                        </Fab>
                    </Tooltip>
                }
            </Grid>


        </Grid>


    </Card >

}

export default Tweet;