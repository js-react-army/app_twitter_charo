import React from 'react'
import {
  AppBar,
  Toolbar,
  Grid,
  Typography,
} from '@material-ui/core'

import TwitterIcon from '@material-ui/icons/Twitter';

import { makeStyles } from '@material-ui/core/styles'

const useStyles = makeStyles((theme) => ({
  headerLogo: {
    display: 'flex',
    alignItems: 'center',
  },
  toolbar: {
    padding: 0,
  },
}))

const Header = () => {
  const classes = useStyles()
  return (
    <AppBar color="inherit" position="static">

      <Toolbar className={classes.toolbar}>
        <Grid
          container
          direction="column"
          justify="space-between"
          alignItems="center"
          style={{ justifyContent: 'center' }}
        >
          <Grid item className={classes.headerLogo} xs={12}>
            <TwitterIcon fontSize="small" style={{ color: 'blue' }} />
          </Grid>
          <Grid item className={classes.headerLogo} xs={12}>
            <Typography variant="h6">Twitter Light</Typography>
          </Grid>
        </Grid>

      </Toolbar>

    </AppBar>
  )
}

export default Header