import React, { useState, useEffect } from 'react'

import { createMuiTheme, ThemeProvider } from '@material-ui/core/styles';

import CssBaseline from '@material-ui/core/CssBaseline'
import { Container } from '@material-ui/core'
import IconButton from '@material-ui/core/IconButton';
import Brightness2 from '@material-ui/icons/Brightness2';
import WbSunny from '@material-ui/icons/WbSunny';

import Header from './components/Header'
import MainTweet from './components/MainTweet'
import ListTweet from './components/ListTweet'

function App() {

  // liste des tweet a afficher
  const [tweets, setTweets] = useState(data)

  //Tweet en cours
  const [objetTweet, setObjetTweet] = useState({
    id: (tweets.length + 1),
    pseudo: "Arthur",
    mail: "@King",
    photo: "arthur.jpg",
    nbLike: 0,
    tweetText: "",
    tweetImage: null,
    owner: true
  })

  // darkMode activation
  const [darkMode, setdarkMode] = useState(false)


//theme
  const theme = createMuiTheme({
    palette: {
      type: darkMode ? "dark" : "light",
    },
    primary: {
      light: '#263238',
      main: '#4f5b62',
      dark: '#000a12',
      contrastText: '#fff',
    },
    secondary: {
      light: '#ff6659',
      main: '#d32f2f',
      dark: '#9a0007',
      contrastText: '#fff',
    },
  });

// Fonction pour changer theme
  const onChangeTheme = () => {
    setdarkMode(!darkMode)
  }

  /**
   * fonction qui ajoute un tweet a la liste des tweets en recuperant le text et l image
   * @param {image} tweetImage 
   * @param {string} tweetText 
   */
  const handleAjout = (tweetImage, tweetText) => {

    // créé une copie de l objet du tweet courant en mettant a jour son text et img
    const objTweet = {
      ...objetTweet,
      tweetText,
      tweetImage,
    }

    // list de tweet intermediaire
    const listTweet = []

    // on push d abord le new tweet
    listTweet.push(objTweet)

    // puis parcours les autres tweet deja present pour les push
    tweets.map(tt => {
      listTweet.push(tt)
    })
    setTweets(listTweet)
  }


  /**
   * fonction qui supprime un tweet selectionne
   * @param {id tweet a suppr} id 
   */
  const deleteTweet = (id) => {
    setTweets(tweets.filter(tt => tt.id !== id))
  }

  // a la MAJ de la liste des tweet on reset le nouvelle Id pour le futur tweet
  useEffect(() => {
    setObjetTweet({
      ...objetTweet,
      id: (tweets.length + 1)
    })
  }, [tweets])


  return (

    <div className="classes.main">
      <ThemeProvider theme={theme}>
        <CssBaseline />
        <Container maxWidth="sm">
          <Header />
          <CssBaseline />
          <MainTweet handleAjout={handleAjout} idNext={objetTweet.id} />
          <CssBaseline />
          <ListTweet data={tweets} deleteTweet={deleteTweet} />
        </Container>
        
        <IconButton aria-label="Brightness2" onClick={onChangeTheme} 
        style={{ borderBottom: 'outset', bordetop: 'outset', right: '20px', bottom: '20px', position: 'fixed' }}>
          {darkMode ? <WbSunny /> : <Brightness2 />}
        </IconButton>

      </ThemeProvider>
    </div>
  );
}





//autres tweets
const data =
  [{
    id: 1,
    pseudo: "Perceval",
    mail: "@Taverne",
    photo: "perceval.jpg",
    nbLike: 1050,
    tweetText: "Donc, pour résumer, je suis souvent victime des colibris, sous-entendu des types qu’oublient toujours tout. Euh, non… Bref, tout ça pour dire, que je voudrais bien qu’on me considère en tant que Tel.",
    tweetImage: null,
    owner: false
  },
  {
    id: 2,
    pseudo: "Karadoc",
    mail: "@Siege d'à coté",
    photo: "karadoc.jpg",
    nbLike: 553,
    tweetText: "Là, vous faites sirop de vingt-et-un et vous dites : beau sirop, mi-sirop, siroté, gagne-sirop, sirop-grelot, passe-montagne, sirop au bon goût.",
    tweetImage: null,
    owner: false
  },
  {
    id: 3,
    pseudo: "Perceval",
    mail: "@Taverne",
    photo: "perceval.jpg",
    nbLike: 1000500,
    tweetText: "En plus, je connais une technique pour tuer trois hommes en un coup rien qu’avec des feuilles mortes ! Alors là, vous êtes deux, vous avez bien de la chance",
    tweetImage: null,
    owner: false
  },
  {
    id: 4,
    pseudo: "Karadoc",
    mail: "@Siege d'à coté",
    photo: "karadoc.jpg",
    nbLike: 10000,
    tweetText: "Vous, vous avez une idée derrière la main, j'en mettrais ma tête au feu !",
    tweetImage: null,
    owner: false
  },
  {
    id: 5,
    pseudo: "Perceval",
    mail: "@Taverne",
    photo: "perceval.jpg",
    nbLike: 50,
    tweetText: "L'agneau était daubé du cul !",
    tweetImage: null,
    owner: false
  },

  ];

export default App;
